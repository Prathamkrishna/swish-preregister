package com.bonfirepreregister.services.impls;

import com.bonfirepreregister.services.interfaces.PreRegisterEmailSenderService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import static com.bonfirepreregister.utils.EmailUtil.registerEmailBody;
import static com.bonfirepreregister.utils.EmailUtil.registerEmailSubject;

@Service
public class PreRegisterEmailSenderServiceImpl implements PreRegisterEmailSenderService {

    private final JavaMailSender javaMailSender;

    public PreRegisterEmailSenderServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public Boolean sendConfirmationEmail(String emailFrom, String username, String emailTo) {
        String emailMessage = "Hey " + username + "!\n\n" + registerEmailBody;
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(emailFrom);
        message.setTo(emailTo);
        message.setSubject(registerEmailSubject);
        message.setText(emailMessage);
        javaMailSender.send(message);
        return null;
    }
}
