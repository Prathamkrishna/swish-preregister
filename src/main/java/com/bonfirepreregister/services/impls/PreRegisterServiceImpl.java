package com.bonfirepreregister.services.impls;

import com.bonfirepreregister.models.PreRegisterUserModel;
import com.bonfirepreregister.repositories.interfaces.CheckUserRegistration;
import com.bonfirepreregister.repositories.interfaces.PreRegisterRepository;
import com.bonfirepreregister.services.interfaces.PreRegisterService;
import com.bonfirepreregister.utils.EmailUtil;
import net.minidev.json.JSONObject;
import org.springframework.stereotype.Service;

import static com.bonfirepreregister.utils.EmailUtil.checkIfValidEmail;

@Service
public class PreRegisterServiceImpl implements PreRegisterService {

    private static final String emailFrom = "noreply@swishteam.com";

    private final PreRegisterRepository preRegisterRepository;
    private final CheckUserRegistration checkUserRegistration;
    private final EmailUtil emailUtil;

    public PreRegisterServiceImpl(PreRegisterRepository preRegisterRepository, CheckUserRegistration checkUserRegistration, EmailUtil emailUtil) {
        this.preRegisterRepository = preRegisterRepository;
        this.checkUserRegistration = checkUserRegistration;
        this.emailUtil = emailUtil;
    }

    @Override
    public JSONObject registerUser(PreRegisterUserModel preRegisterUserModel){
        JSONObject returningJsonObject = new JSONObject();
        returningJsonObject.put("error", false);
        returningJsonObject.put("userAlreadyExists", false);
        returningJsonObject.put("userAdded", false);
        if (!checkIfValidEmail(preRegisterUserModel.getEmail())){
            returningJsonObject.replace("error", "invalidEmail");
            return returningJsonObject;
        }
        if (checkUserRegistration.userExists(preRegisterUserModel.getName(), preRegisterUserModel.getEmail())){
            returningJsonObject.replace("userAlreadyExists", true);
        } else {
            boolean userAdded = preRegisterRepository.preRegisterUser(preRegisterUserModel.getName(), preRegisterUserModel.getEmail());
            returningJsonObject.replace("userAdded", userAdded);
            emailUtil.sendConfirmationEmailFromServer(emailFrom, preRegisterUserModel.getName(), preRegisterUserModel.getEmail());
        }
        return returningJsonObject;
    }

}
