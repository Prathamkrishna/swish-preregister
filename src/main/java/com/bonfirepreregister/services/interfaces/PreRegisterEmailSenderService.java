package com.bonfirepreregister.services.interfaces;

public interface PreRegisterEmailSenderService {
    Boolean sendConfirmationEmail(String emailFrom, String username, String email);
}
