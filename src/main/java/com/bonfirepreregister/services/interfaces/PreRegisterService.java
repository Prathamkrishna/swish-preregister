package com.bonfirepreregister.services.interfaces;

import com.bonfirepreregister.models.PreRegisterUserModel;
import net.minidev.json.JSONObject;

import java.util.concurrent.ExecutionException;

public interface PreRegisterService {
    JSONObject registerUser(PreRegisterUserModel preRegisterUserModel) throws ExecutionException, InterruptedException;
}
