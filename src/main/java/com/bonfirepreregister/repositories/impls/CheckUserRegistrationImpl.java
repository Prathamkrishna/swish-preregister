package com.bonfirepreregister.repositories.impls;

import com.bonfirepreregister.repositories.interfaces.CheckUserRegistration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CheckUserRegistrationImpl implements CheckUserRegistration {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public CheckUserRegistrationImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Boolean userExists(String name, String email) {
        String checkIfUserExistsSql = "Select * from preapplicationusers where name = :name and email = :email";
        Map<String, String> checkIfUserExistsMap = new HashMap<>();
        checkIfUserExistsMap.put("name", name);
        checkIfUserExistsMap.put("email", email);
        List<Map<String, Object>> list = namedParameterJdbcTemplate.queryForList(checkIfUserExistsSql, checkIfUserExistsMap);
        return list.size() != 0;
    }
}
