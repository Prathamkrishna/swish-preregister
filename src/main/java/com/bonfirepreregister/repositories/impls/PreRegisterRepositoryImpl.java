package com.bonfirepreregister.repositories.impls;

import com.bonfirepreregister.repositories.interfaces.PreRegisterRepository;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class PreRegisterRepositoryImpl implements PreRegisterRepository {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public PreRegisterRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Boolean preRegisterUser(String name, String email) {
        String preRegisterSQL = "Insert into preapplicationusers (name, email) values (:name, :email);";
        Map<String, String> preRegisterMap = new HashMap<>();
        preRegisterMap.put("name", name);
        preRegisterMap.put("email", email);
        int addedElements = namedParameterJdbcTemplate.update(preRegisterSQL, preRegisterMap);
        return addedElements != 0;
    }
}
