package com.bonfirepreregister.repositories.interfaces;

public interface PreRegisterRepository {
    Boolean preRegisterUser(String name, String email);
}
