package com.bonfirepreregister.repositories.interfaces;

public interface CheckUserRegistration {
    Boolean userExists(String name, String email);
}
