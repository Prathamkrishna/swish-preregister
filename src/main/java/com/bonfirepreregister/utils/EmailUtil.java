package com.bonfirepreregister.utils;

import com.bonfirepreregister.services.interfaces.PreRegisterEmailSenderService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmailUtil {

    public static final String registerEmailSubject = "All set!";
    public static final String registerEmailBody = "Thank you for pre-registering!\n\nTeam Bonfire.";

    private final PreRegisterEmailSenderService preRegisterEmailSenderService;

    public EmailUtil(PreRegisterEmailSenderService preRegisterEmailSenderService) {
        this.preRegisterEmailSenderService = preRegisterEmailSenderService;
    }

    public static Boolean checkIfValidEmail(String email){
        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Async("emailThreadPool")
    public void sendConfirmationEmailFromServer(String emailFrom, String name, String emailTo){
        preRegisterEmailSenderService.sendConfirmationEmail(emailFrom, name, emailTo);
    }

}
