package com.bonfirepreregister.controllers;

import com.bonfirepreregister.models.PreRegisterUserModel;
import com.bonfirepreregister.services.interfaces.PreRegisterService;
import net.minidev.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api")
public class PreRegisterHandler {

    private final PreRegisterService preRegisterService;

    public PreRegisterHandler(PreRegisterService preRegisterService) {
        this.preRegisterService = preRegisterService;
    }

//    @CrossOrigin(origins = "https://bonefire.vercel.app/, https://jx637.csb.app/")
    @PostMapping("/register")
    public JSONObject preRegisterNewUser(@RequestBody PreRegisterUserModel preRegisterUserModel) throws ExecutionException, InterruptedException {
        return preRegisterService.registerUser(preRegisterUserModel);
    }

//    test
//    @CrossOrigin(origins = {"https://bonefire.vercel.app/, https://jx637.csb.app/"})
    @GetMapping("/test")
    public String testApi(){
        return "Test succeeded";
    }
}
