package com.bonfirepreregister.models;

import lombok.Data;

@Data
public class PreRegisterUserModel {
    private String name;
    private String email;
}
