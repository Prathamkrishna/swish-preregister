package com.bonfirepreregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class BonfirePreregisterApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonfirePreregisterApplication.class, args);
    }

}
